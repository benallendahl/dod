class Item:
    def __init__(self, name, loc):
        self.name = name
        self.loc = loc

items = []

playerLocation = 0
turnCount = 0 
boundaries =   [[True, False, False, False],
                [True, True, False, False],
                [False, False, True, False],
                [True, True, True, False],
                [True, True, True, True],
                [True, False, False, True],
                [False, False, True, False],
                [False, True, True, False],
                [False, False, True, True]]

def initWorld():
    print('Welcome to the Dungeons of Doom!')
    items.append(Item('the Sword of Damocles', 6))
    items.append(Item('the Amulet Ur Hekau Setcheh', 2))
    items.append(Item('the Axe of Exeter', -1))



def describeWorld():
    global playerLocation
    if playerLocation == 0:
        print('You are by a babbling brook.')
    elif playerLocation == 1:
        print('You are high in some misty mountains')
    elif playerLocation == 2:
        print('You are in a dark cave with a dragon')
    elif playerLocation == 3:
        print('You are in a sunny field')
    elif playerLocation == 4:
        print('You are under a great oak tree')
    elif playerLocation == 5:
        print('You are in some murky marshes')
    elif playerLocation == 6:
        print('You are in some deserted tower ruins')
    elif playerLocation == 7:
        print('You are on top of some high cliffs')
    elif playerLocation == 8:
        print('You are on a lonely beach')
    for i in items:
        if playerLocation == i.loc:
            print('You can see', i.name)

def getPlayerCommand():
    cmd=input('What next? ')
    return cmd

def updateWorld(cmd):
    global playerLocation
    if cmd == 'north' or cmd == 'n':
        if boundaries[playerLocation][0]:
            print('You go north')
            playerLocation = playerLocation + 3
        else:
            print('You cannot go north from here')
    elif cmd == 'south' or cmd == 's':
        if boundaries[playerLocation][2]:
            print('You go south')
            playerLocation = playerLocation - 3
        else:
            print('You cannot go south from here')
    elif cmd == 'west' or cmd == 'w':
        if boundaries[playerLocation][3]:
            print('You go west')
            playerLocation = playerLocation - 1
        else:
            print('You cannot go west from here')
    elif cmd == 'east' or cmd == 'e':
        if boundaries[playerLocation][1]:
            print('You go east')
            playerLocation = playerLocation + 1
        else:
            print('You cannot go east from here')
    elif cmd == 'q' or cmd == 'quit':
        quit()
    elif cmd == 'i' or cmd =='inventory':
        for i in items:
            if i.loc == -1:
                print('You are carrying', i.name)
    elif cmd == 't' or cmd == 'take':
        for i in items:
            if playerLocation == i.loc:
                print('You pick up', i.name)
                i.loc = -1
    elif cmd == 'd' or cmd == 'drop':
        for i in items:
            if i.loc == -1:
                print('You put down', i.name)
                i.loc = playerLocation


initWorld()

while True:
    describeWorld()
    cmd = getPlayerCommand()
    print(playerLocation)
    updateWorld(cmd)
